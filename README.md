# dotfiles
This repository contains all of my configuration files for my current Arch Linux installation and an install script so I can quickly replicate my setup.

While the configurations are based on my preferences and made my dotfiles public so others can see how I solved specific problems or are looking for some inspiration.

![Screenshot](./screenshot.png)

## Installation
The installation is pretty straightforward with the help of the included install script.

1. Clone the repository
2. Run `./install`
3. Reboot the system

## Contents
This repository basically contains every important configuration file and script
that I need for my Linux setup. The main content can be found in the `system`
directory. The `packages` directory contains lists of packages of my system that
get installed with the install script.

## Keybindings
| Keybinding                                       | Description                      |
|--------------------------------------------------|----------------------------------|
| <kbd>Super</kbd>+<kbd>1-9</kbd>                  | Switching workspace              |
| <kbd>Super</kbd>+<kbd>Shift</kbd>+<kbd>1-9</kbd> | Move focused window to workspace |
| <kbd>Super</kbd>+<kbd>G</kbd>                    | Open game menu                   |

