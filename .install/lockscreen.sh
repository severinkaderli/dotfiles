#!/usr/bin/env bash
# 
# SCRIPT NAME:
# lockscreen.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Sets the lockscreen image

output::section "Lockscreen"
output::log "Setting lockscreen iamge"
betterlockscreen -u "${DOTFILES}/assets/lockscreen.${HOST}.jpg" |& output::debug
output::success "Successfully set lockscreen image" 