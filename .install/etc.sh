#!/usr/bin/env bash
# 
# SCRIPT NAME:
# etc.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Copies files to /etc

declare -A CONFIG_ETC_FILES
CONFIG_ETC_FILES=(
    ["/etc/cron.daily/update-mirror"]="755"
    ["/etc/default/tlp"]="644"
    ["/etc/docker/daemon.json"]="644"
    ["/etc/gemrc"]="644"
    ["/etc/logrotate.d/custom.conf"]="644"
    ["/etc/mkinitcpio.conf"]="644"
    ["/etc/NetworkManager/conf.d/dns.conf"]="644"
    ["/etc/pacman.conf"]="644"
    ["/etc/resolv.conf"]="644"
    ["/etc/sudoers.d/severin"]="0440"
    ["/etc/vconsole.conf"]="644"
    ["/etc/X11/nvidia-xorg.conf.d/00-keyboard.conf"]="644"
    ["/etc/X11/nvidia-xorg.conf.d/20-displaylink.conf"]="644"
    ["/etc/X11/xorg.conf.d/00-keyboard.conf"]="644"
    ["/etc/X11/xorg.conf.d/20-displaylink.conf"]="644"
    ["/etc/zsh/zshenv"]="644"
)

output::section "Copying /etc files"
for file in "${!CONFIG_ETC_FILES[@]}"; do
    output::log "Copying ${YELLOW}${file}${DEFAULT}"
    sudo install -Dm "${CONFIG_ETC_FILES[${file}]}" "${SYSTEM_DIR}${file}" "${file}" |& output::debug
done
output::success "Successfully copied /etc files"