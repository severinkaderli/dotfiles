#!/usr/bin/env bash
# 
# SCRIPT NAME:
# shell.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Sets default shell

output::section "Shell"
output::log "Setting default shell to zsh"
chsh -s "/bin/zsh"
output::success "Successfully set default shell to zsh"
