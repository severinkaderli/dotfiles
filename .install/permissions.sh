#!/usr/bin/env bash
# 
# SCRIPT NAME:
# groups.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Sets permissions for folders and files

declare -A CONFIG_PERMISSIONS
CONFIG_PERMISSIONS=(
    ["${XDG_BIN_HOME}"]="744"
)

output::section "Permissions"
for permission in "${!CONFIG_PERMISSIONS[@]}"; do
    output::log "Changing owner of ${YELLOW}${permission}${DEFAULT} to ${YELLOW}${USER}${DEFAULT}"
    sudo chown -R "${USER}" "${permission}" |& output::debug
    output::log "Changing permission of ${YELLOW}${permission}${DEFAULT} to ${YELLOW}${CONFIG_PERMISSIONS[${permission}]}${DEFAULT}"
    sudo chmod -R "${CONFIG_PERMISSIONS[${permission}]}" "${permission}" |& output::debug
done
output::success "Successfully set permissions"
