#!/usr/bin/env bash
# 
# SCRIPT NAME:
# prerequisites.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Installs the prerequisites for the installation script.

output::section "Prerequisites"

output::log "Installing requirements for install script"
sudo pacman -S sudo ccache git base-devel --noconfirm --needed |& output::debug
output::success "Requirements successfully installed"

is_yay_installed=$(command -v yay)
output::log "Installing yay"
if [ -z "${is_yay_installed}" ]; then
    TMP_DIR=$(mktemp -d)
    git clone https://aur.archlinux.org/yay.git "${TMP_DIR}" |& output::debug

    (
        cd "${TMP_DIR}"|| exit
        makepkg -si --noconfirm --skippgpcheck |& output::debug
    )

    output::success "yay installed"
else
    output::success "Yay is already installed"
fi