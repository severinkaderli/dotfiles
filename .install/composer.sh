#!/usr/bin/env bash
# 
# SCRIPT NAME:
# composer.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Installs global composer packages

CONFIG_COMPOSER_PACKAGES=(
    "laravel/installer"
)

output::section "Composer"

if output::prompt "Do you want to install global composer packages?"; then
    output::log "Installing ${#CONFIG_COMPOSER_PACKAGES[@]} packages"
    output::list "${CONFIG_COMPOSER_PACKAGES[@]}"
    composer global require "${CONFIG_COMPOSER_PACKAGES[@]}" |& output::debug
    output::success "Packages successfully installed"
fi