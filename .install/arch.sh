#!/usr/bin/env bash
# 
# SCRIPT NAME:
# arch.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Installs arch packages from the offical repositories

output::section "Arch Packages"

# List created using: yay -Qqen > packages.native.list
if output::prompt "Do you want to install Arch packages?"; then
    output::log "Updating package database"
    yay -Syy |& output::debug
    output::success "Package database successfully updated"

    package_count=$(< "${INSTALL_DIR}/packages/packages.native.list" wc -l)
    output::log "Installing ${package_count} packages"
    yay -S \
        --asexplicit \
        --noconfirm \
        --sudoloop \
        --needed \
        --devel \
        --nopgpfetch \
        --mflags \
        --skippgpcheck \
        - < "${INSTALL_DIR}/packages/packages.native.list" |& output::debug
    output::success "Packages successfully installed"
fi
