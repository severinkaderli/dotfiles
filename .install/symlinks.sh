#!/usr/bin/env bash
# 
# SCRIPT NAME:
# symlinks.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Creates symlinks

CONFIG_LINKED_FILES_HOME=(
    ".config/alacritty"
    ".config/autokey"
    ".config/bat"
    ".config/ccache"
    ".config/cmus"
    ".config/compton"
    ".config/cron"
    ".config/custom"
    ".config/chromium-flags.conf"
    ".config/dconf"
    ".config/dunst"
    ".config/git"
    ".config/gtk-2.0"
    ".config/gtk-3.0"
    ".config/httpie"
    ".config/i3"
    ".config/maven"
    ".config/mpd"
    ".config/mpv"
    ".config/MusicBrainz"
    ".config/ncmpcpp"
    ".config/npm"
    ".config/octave"
    ".config/pacman"
    ".config/polybar"
    ".config/python"
    ".config/redshift"
    ".config/sqlite3"
    ".config/streamlink"
    ".config/ssh"
    ".config/sxhkd"
    ".config/topgrade.toml"
    ".config/Trolltech.conf"
    ".config/user-dirs.dirs"
    ".config/user-dirs.locale"
    ".config/vim"
    ".config/vue"
    ".config/wget"
    ".config/X11"
    ".config/yay"
    ".config/zathura"
    ".config/zsh"
    ".local/bin"
    ".local/share/gnupg/gpg-agent.conf"
)

declare -A CONFIG_LINKED_FILES
CONFIG_LINKED_FILES=(
    ["${HOME}/documents"]="${HOME}/data/Documents"
    ["${HOME}/music"]="${HOME}/data/media/Music"
    ["${HOME}/pictures"]="${HOME}/data/media/pictures"
)

output::section "Symlinks"
for file in "${CONFIG_LINKED_FILES_HOME[@]}"; do
    output::log "Linking ${YELLOW}\${HOME}/${file}${DEFAULT} to ${YELLOW}\${SYSTEM_DIR}/${file}${DEFAULT}"
    rm -rf "${HOME:?}/${file}" |& output::debug
    ln -fs "${SYSTEM_DIR}/${file}" "${HOME}/${file}" |& output::debug
done

for file in "${!CONFIG_LINKED_FILES[@]}"; do
    output::log "Linking ${YELLOW}${file}${DEFAULT} to ${YELLOW}${CONFIG_LINKED_FILES[${file}]}${DEFAULT}"
    rm -rf "${file}" |& output::debug
    ln -fs "${CONFIG_LINKED_FILES[${file}]}" "${file}" |& output::debug
done
output::success "Successfully created symlinks"