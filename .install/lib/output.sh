#!/usr/bin/env bash

RESET="$(tput sgr0)"
GREEN="$(tput setaf 2)"
RED="$(tput setaf 1)"
BLUE="$(tput setaf 4)"
DEFAULT="$(tput op)"
YELLOW="$(tput setaf 3)"
BOLD="$(tput bold)"

output::log() {
    echo -e " ● ${1}" |& tee -a "${INSTALL_LOG}"
}

output::debug() {
    if [ -n "${INSTALL_DEBUG}" ]; then
        tee -a "${INSTALL_LOG}"
    else
        cat > "${INSTALL_LOG}"
    fi
}

output::list() {
    for element in "${@}"; do
        echo -e "   - ${element}"
    done
}

output::success() {
    echo -e " ${GREEN}●${DEFAULT} ${1}" |& tee -a "${INSTALL_LOG}"
}

output::question() {
    echo -e " ${YELLOW}${1}${DEFAULT}\c" |& tee -a "${INSTALL_LOG}"
}

output::error() {
    echo -e " ${RED}✘${DEFAULT} ${1}" |& tee -a "${INSTALL_LOG}"
}

output::section() {
    echo -e "\n${BOLD}-- ${1} --${RESET}" |& tee -a "${INSTALL_LOG}"
}

output::prompt() {
    output::question "${1} [y/N] "
    read -n 1 -r
    echo ""

    if [[ ${REPLY} =~ ^[yY]$ ]]; then
        return 0
    fi

    return 1
}