#!/usr/bin/env bash
# 
# SCRIPT NAME:
# fonts.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Setups fonts

output::section "Fonts"
output::log "Prepare xorg fonts"
sudo mkfontdir /usr/share/fonts/75dpi |& output::debug
sudo mkfontdir /usr/share/fonts/100dpi |& output::debug
output::success "Successfully prepared xorg fonts"
