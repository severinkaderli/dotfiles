#!/usr/bin/env bash
# 
# SCRIPT NAME:
# ruby.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Installs ruby gems

CONFIG_RUBY_GEMS=()

output::section "Ruby"

if output::prompt "Do you want to install ruby gems?"; then
    output::log "Installing ${#CONFIG_RUBY_GEMS[@]} gems"
    output::list "${CONFIG_RUBY_GEMS[@]}"
    gem install "${CONFIG_RUBY_GEMS[@]}" |& output::debug
    output::success "Gems successfully installed"
fi