#!/usr/bin/env bash
# 
# SCRIPT NAME:
# groups.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Adds the current user to the configured groups.

CONFIG_GROUPS=(
    "docker"
    "log"
    "lp"
    "mpd"
    "wheel"
    "video"
)

output::section "Groups"
output::log "Current user: ${USER}"
for group in "${CONFIG_GROUPS[@]}"; do
    output::log "Adding user to group ${YELLOW}${group}${DEFAULT}"
    sudo gpasswd -a "${USER}" "${group}" |& output::debug
done
output::success "Successfully added user to groups"