#!/usr/bin/env bash
# 
# SCRIPT NAME:
# npm.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Installs global npm packages

CONFIG_NPM_PACKAGES=(
    "@vue/cli"
    "eslint"
    "gatsby-cli"
    "npm-check-updates"
)

output::section "NPM"

if output::prompt "Do you want to install global npm packages?"; then
    output::log "Installing ${#CONFIG_NPM_PACKAGES[@]} packages"
    output::list "${CONFIG_NPM_PACKAGES[@]}"
    npm i -g "${CONFIG_NPM_PACKAGES[@]}" |& output::debug
    output::success "Packages successfully installed"
fi