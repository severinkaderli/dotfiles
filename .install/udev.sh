#!/usr/bin/env bash
# 
# SCRIPT NAME:
# udev.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Copying custom udev rules

output::section "Copying custom udev rules"
for file in "${SYSTEM_DIR}/etc/udev/rules.d/"*.rules; do
    output::log "Copying ${YELLOW}$(basename "${file}")${DEFAULT} to ${YELLOW}/etc/udev/rules.d/${DEFAULT}"
    sudo cp "${file}" "/etc/udev/rules.d/" |& output::debug
done

output::log "Reload rules"
sudo udevadm control --reload-rules |& output::debug

output::success "Successfully copied udev rules"