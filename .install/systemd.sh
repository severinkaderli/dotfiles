#!/usr/bin/env bash
# 
# SCRIPT NAME:
# systemd.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Copies custom systemd services and enables needed services.

CONFIG_SYSTEMD_SERVICES=(
    "acpid"
    "bluetooth"
    "cronie"
    "docker"
    "NetworkManager"
    "org.cups.cupsd"
    "suspend"
    "systemd-timesyncd"
    "tlp"
    "upower"
)

CONFIG_SYSTEMD_USER_SERVICES=(
    "mpd"
)

output::section "Copying systemd services"
for service in "${SYSTEM_DIR}/etc/systemd/system/"*; do
    output::log "Copying service ${YELLOW}$(basename "${service}")${DEFAULT}"
    sudo install -m 644 "${service}" "/etc/systemd/system" |& output::debug
done
output::success "Successfully copied systemd services"


output::section "Enabling systemd services"
for service in "${CONFIG_SYSTEMD_SERVICES[@]}"; do
    output::log "Enabling service ${YELLOW}${service}${DEFAULT}"
    sudo systemctl enable "${service}" |& output::debug
done

for service in "${CONFIG_SYSTEMD_USER_SERVICES[@]}"; do
    output::log "Enabling service ${YELLOW}${service}${DEFAULT}"
    systemctl --user enable "${service}" |& output::debug
done
output::success "Successfully enabled systemd services"