#!/usr/bin/env bash
# 
# SCRIPT NAME:
# issue.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Creates the /etc/issue file

output::section "Issue"
output::log "Creating /etc/issue"
{
    echo '\e{red}';
    < "/etc/hostname" tr '[:lower:]' '[:upper:]' | figlet -f big | sed "s/\\\\/\\\\\\\/g";
    echo -e "\\\r (\\\l)";
    echo '\e{reset}';
} > "/tmp/issue" 
sudo install "/tmp/issue" "/etc/issue" |& output::debug
output::log "Successfully created /etc/issue"