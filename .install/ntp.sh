#!/usr/bin/env bash
# 
# SCRIPT NAME:
# ntp.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Enables NTP

output::section "NTP"
output::log "Enabling NTP"
sudo timedatectl set-ntp true |& output::debug
output::success "Successfully enabled NTP"