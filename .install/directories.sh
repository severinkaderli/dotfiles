#!/usr/bin/env bash
# 
# SCRIPT NAME:
# directories.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Creates directories in the home directory

CONFIG_DIRECTORIES=(
    ".local/log"
    ".local/share/gnupg"
    ".local/share/vpn"
    "dev/build"
    "dev/opensource"
    "dev/pkg"
    "dev/projects"
    "downloads"
    "games"
    "videos"
)

output::section "Creating directories"

for dir in "${CONFIG_DIRECTORIES[@]}"; do
    output::log "Creating directory ${YELLOW}${HOME}/${dir}${DEFAULT}"
    mkdir -p "${HOME}/${dir}"
done
output::success "Successfully created directories"