#!/usr/bin/env bash
# 
# SCRIPT NAME:
# aur.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Installs arch packages from the AUR

output::section "AUR Packages"

# List created using: yay -Qqem > packages.aur.list
if output::prompt "Do you want to install AUR packages?"; then
    output::log "Updating package database"
    yay -Syy |& output::debug
    output::success "Package database successfully updated"

    package_count=$(< "${INSTALL_DIR}/packages/packages.aur.list" wc -l)
    output::log "Installing ${package_count} packages. This may take a few hours"
    yay -S \
        --asexplicit \
        --noconfirm \
        --sudoloop \
        --needed \
        --devel \
        --nopgpfetch \
        --mflags \
        --skippgpcheck \
        - < "${INSTALL_DIR}/packages/packages.aur.list" |& output::debug
    output::success "Packages successfully installed"
fi