#!/usr/bin/env bash
# 
# SCRIPT NAME:
# rust.sh
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# Install a Rust toolchain and components using Rustup

CONFIG_RUST_CHANNEL="beta"
CONFIG_RUST_COMPONENTS=(
    "clippy"
    "rls"
    "rustfmt"
)

output::section "Rust"

if output::prompt "Do you want to install a Rust toolchain?"; then
    output::log "Installing Rust using ${CONFIG_RUST_CHANNEL} channel"
    rustup -q toolchain install "${CONFIG_RUST_CHANNEL}" |& output::debug
    rustup -q default "${CONFIG_RUST_CHANNEL}" |& output::debug
    output::success "Rust toolchain was installed and activated"

    output::log "Installing components for Rust"
    output::list "${CONFIG_RUST_COMPONENTS[@]}"
    rustup -q component add "${CONFIG_RUST_COMPONENTS[@]}" |& output::debug
    output::success "Rust components were successfully installed"
fi