#!/usr/bin/env bash
# 
# SCRIPT NAME:
# utils
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# This script provides functions and variables that I use in my other scripts.
# It is not supposed to be run directly but to be sourced by other scripts.
#
# USAGE:
# Source this script in the needed script.
#######################################
# Directory variables
#######################################
DIR="$( cd "$(dirname "${BASH_SOURCE[0]}" )" && cd ../../../ && pwd)"
SYSTEM_DIR="${DIR}/system"
PACKAGES_DIR="${DIR}/packages"

#######################################
# Bash color code variables
#######################################
RESET="$(tput sgr0)"
GREEN="$(tput setaf 2)"
RED="$(tput setaf 1)"
BLUE="$(tput setaf 4)"
DEFAULT="$(tput op)"
YELLOW="$(tput setaf 3)"
BOLD="$(tput bold)"

#######################################
# Helper functions
#######################################
# Function to print out a log entry together with the current date and time.
# USAGE: print_time_log SCRIPT MESSAGE
function print_time_log() {
    echo -e "[$(date "+%F %T")][${1}] ${2}"
}

# Function to print a info message with the current time and to send
# a notification using notify-send.
# USAGE: print_notify TITLE MESSAGE
function print_notify() {
    notify-send "${1}" "${2}"
    echo -e "${YELLOW}[$(date "+%T")] ${1} ${2}${DEFAULT}"
}

# Prints a section title.
# USAGE: print_section TITLE
function print_section() {
    echo -e "${BOLD}${BLUE}::${DEFAULT} ${1}${RESET}"
}

function print_log() {
    echo -e "  ${BLUE}->${DEFAULT} ${1}"
}

