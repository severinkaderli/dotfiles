#!/bin/bash
# 
# SCRIPT NAME:
# .zprofile
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# This script is called when you start a login shell.
#
# USAGE:
# This script is automatically executed when you start a login shell.
if [[ -f "${HOME}/.config/zsh/.zshrc" ]]; then
    . "${HOME}/.config/zsh/.zshrc"
fi

if [[ -f "${XDG_CONFIG_HOME}/custom/keyleds" ]]; then
    . "${XDG_CONFIG_HOME}/custom/keyleds"
fi

# Restore custom dconf configuration
dconf load / < "${XDG_CONFIG_HOME}/dconf/root.conf"

# Activate correct crontab file
crontab "${XDG_CONFIG_HOME}/cron/crontab"

# If we are on tty1 we start the x-server
if [ "$(tty)" = "/dev/tty1" ]; then
    startx "${XDG_CONFIG_HOME}/X11/xinitrc" -- -keeptty > "${XDG_LOG_HOME}/xorg.log" 2>&1 && exit
fi

# If we are on tty2 we start the x-server using nvidia-xrun
if [ "$(tty)" = "/dev/tty2" ]; then
    nvidia-xrun > "${XDG_LOG_HOME}/nvidia-xorg.log" 2>&1 && exit && exit
fi
