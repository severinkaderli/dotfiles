#!/bin/bash
# 
# SCRIPT NAME:
# .zshrc
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# This script is called when you start a new shell.
#
# USAGE:
# This script is automatically executed when you start a new shell.

# Zsh options
setopt prompt_subst
unsetopt correct


# Zsh keybindings

# Ctrl + Left
bindkey "^[[1;5C" forward-word
# Ctrl + Right
bindkey "^[[1;5D" backward-word

# Home
bindkey "^[[H" beginning-of-line

# End
bindkey "^[[F" end-of-line


# Enable autocompletion features
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select

# Persistent rehash
zstyle ':completion:*' rehash true

# Use colors from xresources for linux console
if [ "$TERM" = "linux" ]; then
    _SEDCMD='s/.*\*color\([0-9]\{1,\}\).*#\([0-9a-fA-F]\{6\}\).*/\1 \2/p'
    while IFS= read -r line; do
        echo -en "${line}"
    done < <(sed -n "${_SEDCMD}" "${XDG_CONFIG_HOME}/X11/xresources" | awk '$1 < 16 {printf "\\e]P%X%s", $1, $2}')
    clear
fi

# Sourcing environment variables
if [ -f "${HOME}/.config/custom/env" ]; then
    . "${HOME}/.config/custom/env"
fi

# Enable color support of ls
if [ -f "${XDG_CONFIG_HOME}/custom/dircolors" ]; then
    dircolors -b "${XDG_CONFIG_HOME}/custom/dircolors" > /dev/null 2>&1
fi

# Sourcing alias definitions
if [ -f "${XDG_CONFIG_HOME}/custom/aliases" ]; then
    . "${XDG_CONFIG_HOME}/custom/aliases"
fi


# Plugins
source "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"