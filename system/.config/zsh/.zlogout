#!/bin/bash
# 
# SCRIPT NAME:
# .zlogout
#
# AUTHOR:
# Severin Kaderli <severin@kaderli.dev>
#
# DESCRIPTION:
# This script is executed when the login shell exits.
#
# USAGE:
# This script is automatically executed when the login shell exits.

# Clear console output
if [ "${SHLVL}" = 1 ]; then
    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
fi
