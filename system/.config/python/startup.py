import atexit
import os
import readline

XDG_DATA_HOME = os.environ["XDG_DATA_HOME"]

if not XDG_DATA_HOME:
    histfile = os.path.join(os.path.expanduser(
        "~"), ".local", "share", "python", "history")
else:
    histfile = os.path.join(XDG_DATA_HOME, "python", "history")

try:
    readline.set_auto_history(False)
    readline.read_history_file(histfile)
    readline.set_history_length(1000)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, histfile)
